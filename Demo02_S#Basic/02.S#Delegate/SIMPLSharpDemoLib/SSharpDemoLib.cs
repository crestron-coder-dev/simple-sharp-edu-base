﻿using System;
using System.Text;
using Crestron.SimplSharp;                          				// For Basic SIMPL# Classes

namespace SIMPLSharpDemoLib
{
    /*
     * S#委托
     * S#的委托主要用于回调S+的特定函数，同时可获得该执行函数的二次传参
     * 委托的范例如下
     */
    //无参无返回值的委托
    public delegate void DelegateUShortFuncFirst();
    //有参有返回值的委托
    public delegate ushort DelegateUshortFuncSecond(ushort args);

    public class SSharpDemoLib
    {
       //把委托暴露为类属性 属性名称跟属性类型不可以同名！
        public DelegateUShortFuncFirst DeleUShortFuncFirst { get; set; }

        public DelegateUshortFuncSecond DeleUshortFuncSecond { get; set; }
        
        public SSharpDemoLib()
        {
            
        }
        //无参无返回值委托的调用
        public void InvokeDelegateFuncFirst()
        {
            /*
             * 因为S+调用S#时属性注册存在不确定性，
             * 因此必须要判断下属性是否为空
             */
            if (DeleUShortFuncFirst!= null)
            {
                DeleUShortFuncFirst();
            }
        }
        //有参有返回值委托的调用
        public ushort InvokeDelegateFuncSecond(ushort args)
        {
            ushort ret = 0;
            if (DeleUshortFuncSecond!= null)
            {
                ret = DeleUshortFuncSecond(args);
            }
            return ret;
        }

    }

}
