using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;
using SIMPLSharpDemoLib;

namespace UserModule_SIMPLTEST
{
    public class UserModuleClass_SIMPLTEST : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        Crestron.Logos.SplusObjects.DigitalInput TEST;
        SIMPLSharpDemoLib.SSharpDemoLib G_DEMOLIB;
        private void MYTESTFUNC (  SplusExecutionContext __context__ ) 
            { 
            ushort I = 0;
            
            
            __context__.SourceCodeLine = 143;
            I = (ushort) ( 110 ) ; 
            __context__.SourceCodeLine = 144;
            // RegisterDelegate( G_DEMOLIB , DELEUSHORTFUNCFIRST , FUNCFIRSTCALLBACK ) 
            G_DEMOLIB .DeleUShortFuncFirst  = FUNCFIRSTCALLBACK; ; 
            __context__.SourceCodeLine = 145;
            // RegisterDelegate( G_DEMOLIB , DELEUSHORTFUNCSECOND , FUNCSECONDCALLBACK ) 
            G_DEMOLIB .DeleUshortFuncSecond  = FUNCSECONDCALLBACK; ; 
            __context__.SourceCodeLine = 146;
            G_DEMOLIB . InvokeDelegateFuncFirst ( ) ; 
            __context__.SourceCodeLine = 148;
            G_DEMOLIB . InvokeDelegateFuncSecond ( (ushort)( I )) ; 
            
            }
            
        public void FUNCFIRSTCALLBACK ( ) 
            { 
            try
            {
                SplusExecutionContext __context__ = SplusSimplSharpDelegateThreadStartCode();
                
                __context__.SourceCodeLine = 154;
                Trace( "FirstFunc CallBack Run!") ; 
                
                
            }
            finally { ObjectFinallyHandler(); }
            }
            
        public ushort FUNCSECONDCALLBACK ( ushort ARG ) 
            { 
            try
            {
                SplusExecutionContext __context__ = SplusSimplSharpDelegateThreadStartCode();
                
                __context__.SourceCodeLine = 160;
                Trace( "SecondFunc CallBack Arg Is {0:d}", (short)ARG) ; 
                __context__.SourceCodeLine = 162;
                return (ushort)( (ARG + 50)) ; 
                
                
            }
            finally { ObjectFinallyHandler(); }
            }
            
        public SimplSharpString FUNCTHIRDCALLBACK ( SimplSharpString ARG ) 
            { 
            CrestronString RETURNVALUE;
            RETURNVALUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            try
            {
                SplusExecutionContext __context__ = SplusSimplSharpDelegateThreadStartCode();
                
                __context__.SourceCodeLine = 169;
                return ( RETURNVALUE )  .ToString() ; 
                
                
            }
            finally { ObjectFinallyHandler(); }
            }
            
        object TEST_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 200;
                MYTESTFUNC (  __context__  ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    
    public override void LogosSplusInitialize()
    {
        SocketInfo __socketinfo__ = new SocketInfo( 1, this );
        InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
        _SplusNVRAM = new SplusNVRAM( this );
        
        TEST = new Crestron.Logos.SplusObjects.DigitalInput( TEST__DigitalInput__, this );
        m_DigitalInputList.Add( TEST__DigitalInput__, TEST );
        
        
        TEST.OnDigitalPush.Add( new InputChangeHandlerWrapper( TEST_OnPush_0, false ) );
        
        _SplusNVRAM.PopulateCustomAttributeList( true );
        
        NVRAM = _SplusNVRAM;
        
    }
    
    public override void LogosSimplSharpInitialize()
    {
        G_DEMOLIB  = new SIMPLSharpDemoLib.SSharpDemoLib();
        
        
    }
    
    public UserModuleClass_SIMPLTEST ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}
    
    
    
    
    const uint TEST__DigitalInput__ = 0;
    
    [SplusStructAttribute(-1, true, false)]
    public class SplusNVRAM : SplusStructureBase
    {
    
        public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
        
        
    }
    
    SplusNVRAM _SplusNVRAM = null;
    
    public class __CEvent__ : CEvent
    {
        public __CEvent__() {}
        public void Close() { base.Close(); }
        public int Reset() { return base.Reset() ? 1 : 0; }
        public int Set() { return base.Set() ? 1 : 0; }
        public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
    }
    public class __CMutex__ : CMutex
    {
        public __CMutex__() {}
        public void Close() { base.Close(); }
        public void ReleaseMutex() { base.ReleaseMutex(); }
        public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
    }
     public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
