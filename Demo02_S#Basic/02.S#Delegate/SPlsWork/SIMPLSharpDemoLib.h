namespace SIMPLSharpDemoLib;
        // class declarations
         class SSharpDemoLib;
     class SSharpDemoLib 
    {
        // class delegates
        delegate FUNCTION DelegateUShortFuncFirst ( );
        delegate INTEGER_FUNCTION DelegateUshortFuncSecond ( INTEGER args );

        // class events

        // class functions
        FUNCTION InvokeDelegateFuncFirst ();
        INTEGER_FUNCTION InvokeDelegateFuncSecond ( INTEGER args );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        DelegateProperty DelegateUShortFuncFirst DeleUShortFuncFirst;
        DelegateProperty DelegateUshortFuncSecond DeleUshortFuncSecond;
    };

