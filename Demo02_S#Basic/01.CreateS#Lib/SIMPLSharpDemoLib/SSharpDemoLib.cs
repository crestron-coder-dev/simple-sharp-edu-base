﻿using System;
using System.Text;
using Crestron.SimplSharp;                          				// For Basic SIMPL# Classes

namespace SIMPLSharpDemoLib
{
    /*
     * 1、S#是S+的深入，每个S+文件经过快思聪编译器编译后实际为.NET的CS文件，
     * 各位同学可以进入SPI目录详细查看，有一定的模板语法。
     * 2、因S+都是固定的模板，所以在S#层，命名空间、生成的文件必须与S+文件不同
     * 3、S#初始化只能用默认构造函数，无法调用带参数构造函数，且默认构造函数
     * 初始化速度必须很快，如果有耗时初始化，需要另外定义初始化函数。
     */
    public class SSharpDemoLib
    {
        /*一般情况下，需要严格根据变量类型对照表进行传参
         * https://help.crestron.com/simpl_sharp/# chapter 3
         * 可以相互引用的变量类型如下：
         */
        public string buf; //字符串类型

        public SimplSharpString simplSharpStr; //快思聪字符串类型

        public int i; //整数类型

        public int[] arr; //数组类型

        public DemoStruct demoStruct = new DemoStruct(); //结构体类型

        /// <summary>
        /// SIMPL+ can only execute the default constructor. If you have variables that require initialization, please
        /// use an Initialize method
        /// </summary>
        public SSharpDemoLib()
        {
            CrestronConsole.PrintLine("Ctor Run");
            i = 10;
            simplSharpStr = "Lib Init Finish";
            arr = new[] { 1, 2, 3, 4 };
            buf = "Buffer Is Null";
        }
        //定义初始化函数
        public void InitLib(ushort arg)
        {
            CrestronConsole.PrintLine("InitLib Run \n\r Arg Is: {0}",arg);
            i = 10;
            simplSharpStr = "Func Init Run";
            arr = new[] {4, 5, 6, 7};
            buf = "Buf is InPut";
        }
        //定义一个普通函数
        public void Func(string arg)
        {
            CrestronConsole.PrintLine("Func arg is {0}",arg);
        }

    }

    public struct DemoStruct
    {
        public ushort i;
        public string name;
    }
}
