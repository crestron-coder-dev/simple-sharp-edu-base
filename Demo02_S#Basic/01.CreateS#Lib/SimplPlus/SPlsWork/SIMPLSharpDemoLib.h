namespace SIMPLSharpDemoLib;
        // class declarations
         class SSharpDemoLib;
         class DemoStruct;
     class SSharpDemoLib 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION InitLib ( INTEGER arg );
        FUNCTION Func ( STRING arg );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        STRING buf[];
        SIMPLSHARPSTRING simplSharpStr[];
        SIGNED_LONG_INTEGER i;
        SIGNED_LONG_INTEGER arr[];
        DemoStruct demoStruct;

        // class properties
    };

     class DemoStruct 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER i;
        STRING name[];

        // class properties
    };

