using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;
using SIMPLSharpDemoLib;

namespace UserModule_SIMPLTEST
{
    public class UserModuleClass_SIMPLTEST : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        Crestron.Logos.SplusObjects.DigitalInput TEST;
        SIMPLSharpDemoLib.SSharpDemoLib G_DEMOLIB;
        private void MYTESTFUNC (  SplusExecutionContext __context__ ) 
            { 
            uint L_I = 0;
            
            
            __context__.SourceCodeLine = 143;
            L_I = (uint) ( G_DEMOLIB.i ) ; 
            __context__.SourceCodeLine = 144;
            Trace( "string param is {0}", G_DEMOLIB . buf ) ; 
            __context__.SourceCodeLine = 146;
            Trace( "crestron string is {0}", G_DEMOLIB . simplSharpStr ) ; 
            __context__.SourceCodeLine = 148;
            G_DEMOLIB . InitLib ( (ushort)( 20 )) ; 
            __context__.SourceCodeLine = 149;
            Trace( "string param is {0}", G_DEMOLIB . buf ) ; 
            __context__.SourceCodeLine = 151;
            Trace( "crestron string is {0}", G_DEMOLIB . simplSharpStr ) ; 
            __context__.SourceCodeLine = 152;
            G_DEMOLIB . Func ( "S+ Func Invoke") ; 
            
            }
            
        object TEST_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 186;
                MYTESTFUNC (  __context__  ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    
    public override void LogosSplusInitialize()
    {
        SocketInfo __socketinfo__ = new SocketInfo( 1, this );
        InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
        _SplusNVRAM = new SplusNVRAM( this );
        
        TEST = new Crestron.Logos.SplusObjects.DigitalInput( TEST__DigitalInput__, this );
        m_DigitalInputList.Add( TEST__DigitalInput__, TEST );
        
        
        TEST.OnDigitalPush.Add( new InputChangeHandlerWrapper( TEST_OnPush_0, false ) );
        
        _SplusNVRAM.PopulateCustomAttributeList( true );
        
        NVRAM = _SplusNVRAM;
        
    }
    
    public override void LogosSimplSharpInitialize()
    {
        G_DEMOLIB  = new SIMPLSharpDemoLib.SSharpDemoLib();
        
        
    }
    
    public UserModuleClass_SIMPLTEST ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}
    
    
    
    
    const uint TEST__DigitalInput__ = 0;
    
    [SplusStructAttribute(-1, true, false)]
    public class SplusNVRAM : SplusStructureBase
    {
    
        public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
        
        
    }
    
    SplusNVRAM _SplusNVRAM = null;
    
    public class __CEvent__ : CEvent
    {
        public __CEvent__() {}
        public void Close() { base.Close(); }
        public int Reset() { return base.Reset() ? 1 : 0; }
        public int Set() { return base.Set() ? 1 : 0; }
        public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
    }
    public class __CMutex__ : CMutex
    {
        public __CMutex__() {}
        public void Close() { base.Close(); }
        public void ReleaseMutex() { base.ReleaseMutex(); }
        public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
    }
     public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
