﻿using System.Threading;
using System.Runtime.InteropServices;
using System;
using System.Text;
using Crestron.SimplSharp;                          				// For Basic SIMPL# Classes

namespace SIMPLSharpDemoLib
{
    /*
     * 事件
     * 事件是委托的封装
     */
    /*
     * .net自定义委托及通过该委托封装的事件有很多种类型，但是能被S+使用的
     *  通常需要 void [name] (object sender, EventArgs e)这个类型的委托，在系统
     *  内有个EventHandler已经预先定义，如果事件的参数需要自定义，则可以使用
     *  EventHandler<>泛型，自定义事件的参数 如果.net内部进行事件传参在不不需要
     *  自身地址的情况下可以用Action、Func 委托进行事件的传参。
     */

    //定义一个自定义委托 格式必须为obj,obj形式 不然自定义事件名称不能产生回调
    public delegate void MyDelegate(object sender, MyDemoArgs arg);
    //定义一个类返回类型的委托
    public delegate MyDemo MyDemoDelegate(string name);

    public class SSharpDemoLib
    {

        //定义一个事件,使用EVENT关键字,并封装这个委托变为事件
        public event MyDelegate OnMyArgEvent;
        public event EventHandler<MyDemoArgs> OnMyArgsEvent;
        //定义一个无入参纯通知的事件
        public event EventHandler OnMyEvent;

        public SSharpDemoLib()
        {

        }

        //定义一个事件触发函数
        public void FuncInvokeEvent(string arg)
        {
            CrestronConsole.PrintLine("Func arg is {0}", arg);
            //调用无参事件
            if (OnMyEvent != null)
            {
                OnMyEvent.Invoke(this, new EventArgs());
            }
            //调用带参事件
            if (OnMyArgEvent != null)
            {
                OnMyArgEvent.Invoke(this, new MyDemoArgs() { EventID = 111, EventName = "MyEvent Args Invoke" });
            }
        }


    }

    public class MyDemo
    {
        public ushort ID { get; set; }
        public string Name { get; set; }
    }

    //定义一个可以传参的事件参数
    public class MyDemoArgs : EventArgs
    {
        public ushort EventID { get; set; }
        public string EventName { get; set; }

        public MyDemoArgs()
        {
            ;
        }

        public MyDemoArgs(ushort id, string name)
        {
            EventID = id;
            EventName = name;
        }
    }
}
