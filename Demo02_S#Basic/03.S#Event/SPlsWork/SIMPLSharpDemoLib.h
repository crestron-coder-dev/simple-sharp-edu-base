namespace SIMPLSharpDemoLib;
        // class declarations
         class SSharpDemoLib;
         class MyDemo;
         class MyDemoArgs;
     class SSharpDemoLib 
    {
        // class delegates

        // class events
        EventHandler OnMyArgEvent ( SSharpDemoLib sender, MyDemoArgs arg );
        EventHandler OnMyArgsEvent ( SSharpDemoLib sender, MyDemoArgs e );
        EventHandler OnMyEvent ( SSharpDemoLib sender, EventArgs e );

        // class functions
        FUNCTION FuncInvokeEvent ( STRING arg );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class MyDemo 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        INTEGER ID;
        STRING Name[];
    };

     class MyDemoArgs 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        INTEGER EventID;
        STRING EventName[];
    };

