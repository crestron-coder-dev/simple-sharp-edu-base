﻿using System;
using System.Text;
using Crestron.SimplSharp;                          				// For Basic SIMPL# Classes

namespace SIMPLSharpDemoLib
{
       
    //基类的委托
    public delegate void OnColorChangeHandler(object sender,EventArgs e);
    //定义委托
    /*
     * s+ 委托的类型要与S#严格进行对应！具体参见对应表
     */
    public delegate SimplSharpString OnStateChangeDelegate(SimplSharpString state);


    public class CarBase
    {
        /*
         * 构造一个车的基类，包含名称、轮子、颜色等基本方法
         */
   
        //基类的事件
        public event OnColorChangeHandler OnColorChange;
        //只读属性   
        public ushort Wheels{get { return 4; }}
        //可读写属性
        public string Name { get; set; }
        //带方法的属性
        private string _color = "white";

        public string Color
        {
            get { return _color; }
            set
            {
                if (value!=_color)
                {
                    _color = value;
                    if (OnColorChange!=null)
                    {
                        OnColorChange(this,new EventArgs());
                    }

                }
                
            }
        }
        //包含2个构造函数，一个带参，一个无参数
        public CarBase()
        {
            
        }
        public CarBase(string name)
        {
            Name = name;
        }

        public virtual void Run()
        {
            CrestronConsole.PrintLine("car is run");
        }
    }

    public class Jeep : CarBase
    {
        //jeep 继承 CarBase，重写了RUN方法
        public Jeep()
        {
            
        }

        public Jeep(string name):base(name)
        {
            Name = string.Format("jeep_{0}", name);
        }

        public override void Run()
        {
            base.Run();
            CrestronConsole.PrintLine("jeep is run");
        }
    }

    public class CarState : EventArgs
    {
        public string State { get; set; }
        public CarState()
        {
            
        }

        public CarState(string state)
        {
            State = state;
        } 
    }

    public class Bumblebee : CarBase
    {
        /*
         * 变形金刚中的大黄蜂：
         * 添加了状态属性与变形方法
         * 重写了RUN方法
         * 事件是委托的封装
         */
        //EventHandler<>是个泛型委托，可以定义args的类型
        public event EventHandler OnStateChange1;
        public event EventHandler<CarState> OnStateChange2;
        /*
         * 使用委托进行参数传递：
         * 定义委托
         * 定义属性为委托属性
         * 定义S+的接受方法
         */

        //定义委托属性
        public OnStateChangeDelegate OnStateChage3 { get; set; }
        private string _state = "";
        public string State {
            get { return _state; }
            set
            {
                if (_state!= value)
                {
                    _state = value;
                    if (OnStateChange1 != null)
                    {
                        //无参数的 eventargs
                        OnStateChange1.Invoke(this, new EventArgs());
                    }
                    if (OnStateChange2 != null)
                    {
                        //带参数的 eventargs
                        OnStateChange2.Invoke(this, new CarState(value));
                    }
                    if (OnStateChage3!= null)
                    {
                        SimplSharpString res = OnStateChage3(value);
                        CrestronConsole.PrintLine(res.ToString());
                    }

                } 
            }
        }

        public Bumblebee()
        {
            
        }

        public Bumblebee(string name, string state) : base(name)
        {
            State = state;
        }

        public void TransformState(string state)
        {
            State = state;
            Run();
        }


        public override void Run()
        {
            //重写run方法
            switch (State)
            {
                case "people":
                    CrestronConsole.PrintLine("state people is runing");
                    break;
                case "car":
                    CrestronConsole.PrintLine("state car is runing");
                    break;
                case "other":
                    CrestronConsole.PrintLine("{0} is dead",Name);
                    break;
                    
            }
           
        }
    }

}
