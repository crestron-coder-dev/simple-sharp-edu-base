namespace SIMPLSharpDemoLib;
        // class declarations
         class CarBase;
         class Jeep;
         class CarState;
         class Bumblebee;
     class CarBase 
    {
        // class delegates

        // class events
        EventHandler OnColorChange ( CarBase sender, EventArgs e );

        // class functions
        FUNCTION Run ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        INTEGER Wheels;
        STRING Name[];
        STRING Color[];
    };

     class Jeep 
    {
        // class delegates

        // class events
        EventHandler OnColorChange ( Jeep sender, EventArgs e );

        // class functions
        FUNCTION Run ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        INTEGER Wheels;
        STRING Name[];
        STRING Color[];
    };

     class CarState 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING State[];
    };

     class Bumblebee 
    {
        // class delegates
        delegate SIMPLSHARPSTRING_FUNCTION OnStateChangeDelegate ( SIMPLSHARPSTRING state );

        // class events
        EventHandler OnStateChange1 ( Bumblebee sender, EventArgs e );
        EventHandler OnStateChange2 ( Bumblebee sender, CarState e );
        EventHandler OnColorChange ( Bumblebee sender, EventArgs e );

        // class functions
        FUNCTION TransformState ( STRING state );
        FUNCTION Run ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        DelegateProperty OnStateChangeDelegate OnStateChage3;
        STRING State[];
        INTEGER Wheels;
        STRING Name[];
        STRING Color[];
    };

