namespace GameOfLife;
        // class declarations
         class GOL;
     class GOL 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        SIGNED_LONG_INTEGER_FUNCTION GetCellState ( SIGNED_LONG_INTEGER row , SIGNED_LONG_INTEGER column );
        FUNCTION NextGeneration ();
        FUNCTION SetCellState ( SIGNED_LONG_INTEGER row , SIGNED_LONG_INTEGER column );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

