﻿using System;
using System.Text;
using Crestron.SimplSharp;                          				// For Basic SIMPL# Classes

namespace GameOfLife
{
    public class GOL
    {

        /// <summary>
        /// SIMPL+ can only execute the default constructor. If you have variables that require initialization, please
        /// use an Initialize method
        /// </summary>

        private const int gridSize = 15;

        private bool[,] grid = new bool[gridSize, gridSize];


        private bool[,] tempGrid = new bool[gridSize, gridSize];


        public GOL()
        {
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    grid[i, j] = false;
                    tempGrid[i, j] = false;
                }
            }
        }

        
        public void Clear()
        {
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    grid[i, j] = false;
                    tempGrid[i, j] = false;
                }
            }
        }

        public int GetCellState(int row, int column)
        {
            return (grid[row, column] ? 1 : 0);
        }

        public void NextGeneration()
        {
            int numLiveNeighbors;
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    //calc live neighbors
                    /*每个细胞的状态由该细胞及周围8个细胞上一次的状态所决定；
                     *如果一个细胞周围有3个细胞为生，则该细胞为生，即该细胞若原先为死则转为生，若原先为生则保持不变；
                     *如果一个细胞周围有2个细胞为生，则该细胞的生死状态保持不变；
                     *在其它情况下，该细胞为死，即该细胞若原先为生则转为死，若原先为死则保持不变。
                     */
                    numLiveNeighbors = 0;
                    for (int iloc = -1; iloc <= 1; iloc++)
                    {
                        for (int jloc = -1; jloc <= 1; jloc++)
                        {
                            if (!(iloc == 0 && jloc == 0))
                            {
                                //周围细胞定位
                                int inbr = (iloc + i + gridSize) % gridSize;
                                int jnbr = (jloc + j + gridSize) % gridSize;
                                
                                if (grid[inbr, jnbr] == true)
                                {
                                    numLiveNeighbors++;
                                }
                            }
                        }
                    }
                    if (grid[i, j])
                    {
                        if ((numLiveNeighbors == 2 || numLiveNeighbors == 3))
                        {
                            tempGrid[i, j] = true;
                        }
                        else
                        {
                            tempGrid[i, j] = false;
                        }
                    }
                    if (grid[i, j])
                    {
                        if (numLiveNeighbors == 3)
                        {
                            tempGrid[i, j] = true;
                        }
                    }
                }
            }
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    grid[i, j] = tempGrid[i, j];
                    tempGrid[i, j] = false;
                }
            }
        }

        public void SetCellState(int row, int column)
        {
            grid[row, column] = !grid[row, column];
        }
    }
}
