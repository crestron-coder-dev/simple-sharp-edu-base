Import-Module PSCrestron
$ip = "192.168.0.236"
$user = "crestron"
$passwd = "crestron"


# 发送单个文件
# $localFile = ".\testfile.txt"
# $remoteFile = "\user\testfile.txt"

# Send-FTPFile $ip $localFile $remoteFile -Secure -Username $user -Password $passwd

# 发送文件目录
# 获取当前目录，并过滤文件结尾
$Files = Get-ChildItem "*.*" -Recurse
# 发送整个目录的文件
ForEach ($File in $Files) {
    # 休眠1秒
    # Start-Sleep -Seconds 1
    Write-Output "send file $($File.Name)"
    Send-FTPFile $ip $File.FullName "\user\$($File.Name)" -Secure -Username $user -Password $passwd
}
