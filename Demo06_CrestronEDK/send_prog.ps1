Import-Module PSCrestron
# powershell 基本学习地址：
# https://learn.microsoft.com/zh-cn/powershell/scripting/lang-spec/chapter-02?view=powershell-7.3
# SDK查看地址：https://sdkcon78221.crestron.com/sdk/Crestron_EDK_SDK/Content/Topics/PSCrestron.htm
$ip = "192.168.0.236"
$user = "crestron"
$passwd = "crestron"
$file = "D:\MyWork\Teacher\simple-sharp-edu-example\9.Python\SimplPy\PythonExampleProgram.lpz"
Send-CrestronProgram -Device $ip -LocalFile $file -ProgramSlot 2 -SendSIGFile -Secure -Username $user -Password $passwd -ShowProgress