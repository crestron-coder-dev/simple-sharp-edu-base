Import-Module PSCrestron
# powershell 基本学习地址：
# https://learn.microsoft.com/zh-cn/powershell/scripting/lang-spec/chapter-02?view=powershell-7.3
# SDK查看地址：https://sdkcon78221.crestron.com/sdk/Crestron_EDK_SDK/Content/Topics/PSCrestron.htm
$ip = "192.168.0.236"
$user = "crestron"
$passwd = "crestron"

$session = Open-CrestronSession -Device $ip -Secure -Username $user -Password $passwd
# 变量可以单步传递，也可以数组传递
$cmd = "errlog"
# $cmd = "errlog", "help all"

Invoke-CrestronSession $session -Command $cmd

Close-CrestronSession $session