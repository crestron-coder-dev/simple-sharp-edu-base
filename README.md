# SimpleSharpEduBase

#### 介绍
快思聪系统使用S#开发的入门教程
   本社群正在筹备Crestron驱动开发高级班，使用.NET语言与Crestron设备进行交互
通过学习此开发，您可以获得Crestron以下能力：
1. 访问WebSocket，Https等协议的能力
2. 快速解析JSON字典的能力
3. 极大提高系统性能的能力
4. 强大的动态界面驱动能力
#### 使用说明

1. 本教程需要安装visual studio 2008(3系列) 及 visual studio2022(4系列及vc-4)
2. 本教程建立在.net framework compact 3.5(3系列) 或 .net mono framework4.7(4系列及vc-4) 
3. 本教程需熟悉Crestron SimpleWindows SimplePlus 及 ToolBox等相关调试软件 
4. 如有意向参加课程请联系WX:ControlMaster1024


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
