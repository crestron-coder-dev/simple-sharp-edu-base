﻿using System;
using System.Text;
using Crestron.SimplSharp;                          				// For Basic SIMPL# Classes

namespace WaitCall
{
    public class WaitcallTest
    {

        /// <summary>
        /// SIMPL+ can only execute the default constructor. If you have variables that require initialization, please
        /// use an Initialize method
        /// </summary>
        public WaitcallTest()
        {

            
        }

        public void CallBack()
        {
            CrestronConsole.PrintLine("callback invoke!");
//            throw new Exception("call back ex");
        }
    }
}
