using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;
using WaitCall;

namespace UserModule_TESTWAIT
{
    public class UserModuleClass_TESTWAIT : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        Crestron.Logos.SplusObjects.DigitalInput D_INPUT;
        Crestron.Logos.SplusObjects.DigitalInput D_INPUT1;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> D_INPUT2;
        Crestron.Logos.SplusObjects.AnalogInput A_INPUT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> A_INPUT2;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> S_INPUT1;
        Crestron.Logos.SplusObjects.DigitalOutput D_OUT;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> D_OUT1;
        Crestron.Logos.SplusObjects.AnalogOutput A_OUT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> A_OUT1;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> S_OUT;
        WaitCall.WaitcallTest GWAITCALL;
        MYSTRUCT1 STRUCT;
        ushort I = 0;
        CrestronString S;
        private void SOME_FUNCTION (  SplusExecutionContext __context__, ushort VAR1 , ushort VAR2 , CrestronString VAR3 ) 
            { 
            ushort LOCALINT = 0;
            
            CrestronString LOCALSTR;
            LOCALSTR  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            
            
            __context__.SourceCodeLine = 167;
            VAR1 = (ushort) ( (VAR1 + 1) ) ; 
            __context__.SourceCodeLine = 168;
            LOCALINT = (ushort) ( (VAR1 + VAR2) ) ; 
            __context__.SourceCodeLine = 169;
            LOCALSTR  .UpdateValue ( Functions.Left ( VAR3 ,  (int) ( 10 ) )  ) ; 
            
            }
            
        private ushort FUNCINT (  SplusExecutionContext __context__, ushort I ) 
            { 
            ushort RETURNVALUE = 0;
            
            
            __context__.SourceCodeLine = 174;
            RETURNVALUE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 175;
            return (ushort)( RETURNVALUE) ; 
            
            }
            
        private CrestronString FUNCSTR (  SplusExecutionContext __context__, CrestronString S ) 
            { 
            CrestronString RETURNVALUE;
            RETURNVALUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            
            __context__.SourceCodeLine = 181;
            RETURNVALUE  .UpdateValue ( S  ) ; 
            __context__.SourceCodeLine = 182;
            return ( RETURNVALUE ) ; 
            
            }
            
        object D_INPUT_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 190;
                Trace( "string {0:d} input1:{1:d}", (short)D_INPUT  .Value, (short)D_INPUT1  .Value) ; 
                __context__.SourceCodeLine = 191;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (D_INPUT1  .Value == 1))  ) ) 
                    { 
                    __context__.SourceCodeLine = 194;
                    return  this ; 
                    } 
                
                __context__.SourceCodeLine = 196;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (D_INPUT1  .Value == 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 199;
                    return  this ; 
                    } 
                
                __context__.SourceCodeLine = 201;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (D_INPUT  .Value == 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 204;
                    return  this ; 
                    } 
                
                __context__.SourceCodeLine = 206;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (D_INPUT  .Value == 1))  ) ) 
                    { 
                    __context__.SourceCodeLine = 209;
                    return  this ; 
                    } 
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object D_INPUT2_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            ushort I = 0;
            
            
            __context__.SourceCodeLine = 216;
            I = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
            __context__.SourceCodeLine = 217;
            Trace( "last d_input {0:d}", (short)D_INPUT2[ I ] .Value) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object D_INPUT_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 222;
        I = (ushort) ( 10 ) ; 
        __context__.SourceCodeLine = 224;
        Trace( "d_input, invoke") ; 
        __context__.SourceCodeLine = 225;
        GWAITCALL . CallBack ( ) ; 
        __context__.SourceCodeLine = 226;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_0__" , 150 , __SPLS_TMPVAR__WAITLABEL_0___Callback ) ;
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_0___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 227;
            I = (ushort) ( 20 ) ; 
            __context__.SourceCodeLine = 229;
            GWAITCALL . CallBack ( ) ; 
            __context__.SourceCodeLine = 230;
            Trace( "wait finish {0:d}", (short)I) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object D_INPUT2_OnChange_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 238;
        Trace( "d_input, invoke {0:d}", (short)D_INPUT2[ 1 ] .Value) ; 
        __context__.SourceCodeLine = 239;
        Trace( "a_input, invoke {0:d}", (short)A_INPUT2[ 1 ] .UshortValue) ; 
        __context__.SourceCodeLine = 240;
        Trace( "s_input, invoke {0}", S_INPUT1 [ 1 ] ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object Event ( Object __EventInfo__ )
    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 245;
        Trace( "d_input, invoke {0:d}", (short)D_INPUT2[ 1 ] .Value) ; 
        __context__.SourceCodeLine = 246;
        Trace( "a_input, invoke {0:d}", (short)A_INPUT2[ 1 ] .UshortValue) ; 
        __context__.SourceCodeLine = 247;
        Trace( "s_input, invoke {0}", S_INPUT1 [ 1 ] ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    ushort I = 0;
    
    CrestronString S;
    S  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 254;
        I = (ushort) ( FUNCINT( __context__ , (ushort)( 100 ) ) ) ; 
        __context__.SourceCodeLine = 255;
        S  .UpdateValue ( FUNCSTR (  __context__ , "aabbcc")  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    S  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    STRUCT  = new MYSTRUCT1( this, true );
    STRUCT .PopulateCustomAttributeList( false );
    
    D_INPUT = new Crestron.Logos.SplusObjects.DigitalInput( D_INPUT__DigitalInput__, this );
    m_DigitalInputList.Add( D_INPUT__DigitalInput__, D_INPUT );
    
    D_INPUT1 = new Crestron.Logos.SplusObjects.DigitalInput( D_INPUT1__DigitalInput__, this );
    m_DigitalInputList.Add( D_INPUT1__DigitalInput__, D_INPUT1 );
    
    D_INPUT2 = new InOutArray<DigitalInput>( 5, this );
    for( uint i = 0; i < 5; i++ )
    {
        D_INPUT2[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( D_INPUT2__DigitalInput__ + i, D_INPUT2__DigitalInput__, this );
        m_DigitalInputList.Add( D_INPUT2__DigitalInput__ + i, D_INPUT2[i+1] );
    }
    
    D_OUT = new Crestron.Logos.SplusObjects.DigitalOutput( D_OUT__DigitalOutput__, this );
    m_DigitalOutputList.Add( D_OUT__DigitalOutput__, D_OUT );
    
    D_OUT1 = new InOutArray<DigitalOutput>( 10, this );
    for( uint i = 0; i < 10; i++ )
    {
        D_OUT1[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( D_OUT1__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( D_OUT1__DigitalOutput__ + i, D_OUT1[i+1] );
    }
    
    A_INPUT = new Crestron.Logos.SplusObjects.AnalogInput( A_INPUT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( A_INPUT__AnalogSerialInput__, A_INPUT );
    
    A_INPUT2 = new InOutArray<AnalogInput>( 10, this );
    for( uint i = 0; i < 10; i++ )
    {
        A_INPUT2[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( A_INPUT2__AnalogSerialInput__ + i, A_INPUT2__AnalogSerialInput__, this );
        m_AnalogInputList.Add( A_INPUT2__AnalogSerialInput__ + i, A_INPUT2[i+1] );
    }
    
    A_OUT = new Crestron.Logos.SplusObjects.AnalogOutput( A_OUT__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( A_OUT__AnalogSerialOutput__, A_OUT );
    
    A_OUT1 = new InOutArray<AnalogOutput>( 10, this );
    for( uint i = 0; i < 10; i++ )
    {
        A_OUT1[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( A_OUT1__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( A_OUT1__AnalogSerialOutput__ + i, A_OUT1[i+1] );
    }
    
    S_INPUT1 = new InOutArray<StringInput>( 10, this );
    for( uint i = 0; i < 10; i++ )
    {
        S_INPUT1[i+1] = new Crestron.Logos.SplusObjects.StringInput( S_INPUT1__AnalogSerialInput__ + i, S_INPUT1__AnalogSerialInput__, 10, this );
        m_StringInputList.Add( S_INPUT1__AnalogSerialInput__ + i, S_INPUT1[i+1] );
    }
    
    S_OUT = new InOutArray<StringOutput>( 30, this );
    for( uint i = 0; i < 30; i++ )
    {
        S_OUT[i+1] = new Crestron.Logos.SplusObjects.StringOutput( S_OUT__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( S_OUT__AnalogSerialOutput__ + i, S_OUT[i+1] );
    }
    
    __SPLS_TMPVAR__WAITLABEL_0___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_0___CallbackFn );
    
    D_INPUT.OnDigitalPush.Add( new InputChangeHandlerWrapper( D_INPUT_OnPush_0, false ) );
    D_INPUT1.OnDigitalPush.Add( new InputChangeHandlerWrapper( D_INPUT_OnPush_0, false ) );
    for( uint i = 0; i < 5; i++ )
        D_INPUT2[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( D_INPUT2_OnPush_1, false ) );
        
    D_INPUT.OnDigitalPush.Add( new InputChangeHandlerWrapper( D_INPUT_OnPush_2, false ) );
    for( uint i = 0; i < 5; i++ )
        D_INPUT2[i+1].OnDigitalChange.Add( new InputChangeHandlerWrapper( D_INPUT2_OnChange_3, false ) );
        
    for( uint i = 0; i < 10; i++ )
        S_INPUT1[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( D_INPUT2_OnChange_3, false ) );
        
    for( uint i = 0; i < 10; i++ )
        A_INPUT2[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( D_INPUT2_OnChange_3, false ) );
        
    OnEventStatement.Add( new InputChangeHandlerWrapper( Event, false ) );
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    GWAITCALL  = new WaitCall.WaitcallTest();
    
    
}

public UserModuleClass_TESTWAIT ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_0___Callback;


const uint D_INPUT__DigitalInput__ = 0;
const uint D_INPUT1__DigitalInput__ = 1;
const uint D_INPUT2__DigitalInput__ = 2;
const uint A_INPUT__AnalogSerialInput__ = 0;
const uint A_INPUT2__AnalogSerialInput__ = 1;
const uint S_INPUT1__AnalogSerialInput__ = 11;
const uint D_OUT__DigitalOutput__ = 0;
const uint D_OUT1__DigitalOutput__ = 1;
const uint A_OUT__AnalogSerialOutput__ = 0;
const uint A_OUT1__AnalogSerialOutput__ = 1;
const uint S_OUT__AnalogSerialOutput__ = 11;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class MYSTRUCT1 : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  I = 0;
    
    [SplusStructAttribute(1, false, false)]
    public CrestronString  S;
    
    
    public MYSTRUCT1( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        S  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, Owner );
        
        
    }
    
}

}
