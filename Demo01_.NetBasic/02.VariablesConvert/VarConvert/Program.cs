﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace VarConvert
{
    //类型转换
    class Program
    {
        static void Main(string[] args)
        {
            var t = new Test();
            
        }
    }

    public class Test
    {
        private string str = "123.25";
        private string intStr = "123";

        public Test()
        {
            //隐式转换
            object o = str;
            Console.WriteLine("type is {0}",o.GetType());
            //强制转换
            object o1 = (object) str;
            Console.WriteLine("type is {0}",o1.GetType());
            //使用as非强制转换
            string o2 = o1 as string;
            //使用parse解析字符串转换
            var i = int.Parse(intStr);
            var f = float.Parse(str);
            //浮点转整数
            var i1 = Convert.ToInt32(f);
            Console.WriteLine(i);
            Console.WriteLine(f);
            Console.WriteLine(i1);
            Console.ReadKey();
        }
    }
}
