﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Property
{
    class Car
    {
        /*
         * 属性的标准写法:
         * 先写私有变量，再写属性，注意变量格式及大小写
         */
        private ushort _carLights = 4;
        private ushort _door = 4;
        public ushort Door{
            get { return _door; }
            set { _door = value; }
        }
        //属性简写
        public ushort Horn { get; set; }
        //只读属性复杂写法
        public string Lottery { get { return "No.Bx88888"; } }
        //调用方法的属性
        public ushort Lights
        {
            //设置2个特殊方法，
            get { return GetCarLights(); }
            set{SetCarLights(value);}
        }
        //虚属性，可以重写
        public virtual string VBaseProp {
            get { return "vbaseprop"; }
            set{}
        }

        public Car()
        {
            
        }

        public void Run()
        {
            
        }

        private void SetCarLights(ushort lights)
        {
            if (lights<10)
            {
                _carLights = lights;
            }
        }
        private ushort GetCarLights()
        {
            //默认是INT类型，转换到USHORT类型
            return Convert.ToUInt16(_carLights*5) ;
        }
    }

    class Jeep:Car
    {
        public override string VBaseProp
        {
            get { return "VChild Prop"; }
        }
        public Jeep(){}

    }

    class Program
    {
        static void Main(string[] args)
        {
            var car = new Car();
            car.Horn = 4;
            Console.WriteLine(car.Lights);
            car.Lights = 2;
            Console.WriteLine(car.Lights);
            car.Lights = 20;
            Console.WriteLine(car.Lights);
            car.Lights = 8;
            Console.WriteLine(car.Lights);
            Console.WriteLine(car.VBaseProp);
            var jeep = new Jeep();
            Console.WriteLine(jeep.Lights);
            Console.WriteLine(jeep.VBaseProp);
            Console.ReadKey();
        }
    }
}
