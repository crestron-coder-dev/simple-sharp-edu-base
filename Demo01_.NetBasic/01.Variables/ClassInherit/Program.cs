﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassInherit
{
    public class Father
    {
        public Father(string name)
        {
            Console.WriteLine("0 father {0} create",name);
        }

        public void Say()
        {
            Console.WriteLine("1 i'm father");
        }

        public virtual void Sing(string song)
        {
            Console.WriteLine("2 father sing a {0}",song);
        }
    }

    public class Child : Father
    {
        public Child(string name):base(name)
        {
            Console.WriteLine("0 child {0} create",name);
        }

        public new void Say()
        {
            //子类覆盖父类方法要加new关键字
            Console.WriteLine("1 i'm child");
        }
        public override void Sing(string song)
        {
            //子类重写父类方法要父类方法用抽象，子类重写
            Console.WriteLine("2 child sing {0}",song);
            //子类调用父类方法
            base.Sing("3 father sing");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //父类实例化
            Father father = new Father("Jack");
            father.Say();
            father.Sing("i love u");
            //子类实例化
            Child child = new Child("Tim");
            child.Say();
            child.Sing("apple red");
            //多态：子类指向父类对象
            Father f = new Child("fg");
            f.Say();
            //子类指向父类时，没有override 的方法，使用父类方法，有override时，使用子类方法
            f.Sing("fg sing");
            Console.ReadKey();
        }
    }
}
