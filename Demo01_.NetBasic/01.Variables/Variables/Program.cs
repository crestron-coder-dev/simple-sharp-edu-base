﻿//引用
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//命名空间
namespace Variables
{
    //类名
    class Program
    {
        //静态方法
        static void Main(string[] args)
        {
            /*
             * 1、注释：
             * 单行注释、多行注释
             * 
             * 2、命名规范：
             * 命名空间、方法、属性使用大驼峰命名法（大写字母开头）
             * 变量使用小驼峰命名法（小写字母开头）
             * 
            */
            
            MyVariables myVariablesFirst = new MyVariables();
            //指定类型
            Console.WriteLine("Class 1 Run");
            myVariablesFirst.Idx = 1;
            myVariablesFirst.Uidx = 1;
            myVariablesFirst.FuncFirst();
            myVariablesFirst.FuncSecond(1);

            Console.WriteLine("Class 2 Run");
            var myVariablesSecond = new MyVariables(2);
            //隐式变量（自动推断变量类型)
            //函数重载（同个函数名称不同的参数及返回值）
            Console.WriteLine(myVariablesSecond.Idx);
            myVariablesSecond.FuncFirst();
            var res2 = myVariablesSecond.FuncSecond(2);
            Console.WriteLine("res is {0}",res2);
            Console.WriteLine("finish");
            Console.ReadKey();

        }
    }
    //类定义
    public class MyVariables
    {
        /*
         * 类是一个复杂数据类型的模板
         */
        private int _idx; //私有变量
        public ushort uidx; //公有变量
        public ushort Uidx { get; set; } //自动属性
        //自定义属性 
        public int Idx
        {
            get { return _idx; }
            set { _idx = value; }
        }
        /// <summary>
        /// 无参构造函数
        /// </summary>
        public MyVariables()
        {
            Console.WriteLine("Ctor Run");
        }
        /// <summary>
        /// 带参构造函数
        /// </summary>
        /// <param name="arg"></param>
        public MyVariables(int arg)
        {
            Console.WriteLine("Ctor Run{0}",arg);
            _idx = arg;
        }
        /// <summary>
        /// 一个无参数，无返回值方法
        /// </summary>
        public void FuncFirst()
        {
            Console.WriteLine("First Function Run");
        }
        /// <summary>
        /// 一个有参数，有返回值的方法
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public int FuncSecond(int arg)
        {
            Console.WriteLine("Second Function Run");
            return arg;
        }

        public static int FuncThird(int arg)
        {
            Console.WriteLine("Third Function Run");
            return arg;
        }

    }
               
}
