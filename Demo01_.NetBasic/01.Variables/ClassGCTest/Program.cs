﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassTest
{
    public class Car
    {
        public Car()
        //构造函数
        {
            Console.WriteLine("car create");
        }

        ~Car()
        //析构函数
        {
            Console.WriteLine("car collect");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car();
            Console.WriteLine("test finish");
            //当创建的实例指向为null时，启动垃圾回收
            car = null;
            GC.Collect();
            Console.WriteLine("gc collect finish");
            //垃圾回收时，可以看到析构函数产生作用
            Console.ReadKey();
        }
    }
}
