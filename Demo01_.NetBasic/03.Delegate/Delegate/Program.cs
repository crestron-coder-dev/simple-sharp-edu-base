﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//委托、匿名方法、lambda
/*
 * 委托是一个类，它定义了方法的类型，
 * 使得可以将方法当作另一个方法的参数来进行传递，
 * 这种将方法动态地赋给参数的做法，
 * 可以避免在程序中大量使用If-Else(Switch)语句，
 * 同时使得程序具有更好的可扩展性。
 * 
 */
namespace Delegate
{        
    //无入参，无返回值的委托
    public delegate void TDelegate1();
    //有入参，有返回值的委托
    public delegate int TDelegate2(int arg);

    class Program
    {
        
        static void Main(string[] args)
        {
            //委托的调用
            var t = new Test();
            t.Func4(
                () =>
                {
                    Console.WriteLine("Lambda1 Run");
                },
                //入参2
                (i) =>
                {
                    Console.WriteLine("Lambda2 Run");
                    return i;
                },
                //入参3
                30
                );
            //使用Action委托
            Action<int> a1 = new Action<int>(x => { Console.WriteLine("Action Invoke args:{0}", x); });
            a1.Invoke(40);
            //使用Func委托
            Func<int,int> f1 = new Func<int, int>(x=> { return x; });
            Console.WriteLine("F1 Result Is: {0}",f1.Invoke(50));
            //匿名方法
            Func<string, string> f2 = delegate(string param)
            {
                Console.WriteLine("Param is: {0}", param);
                return param;
            };
            //有委托的地方大部分可以用Lambda表达式方式进行方法定义
            Console.WriteLine("F2 Result Is: {0}",f2.Invoke("This is F2 Invoke!"));
            Console.ReadKey();
        }
        
    }

    public class Test
    {
        
        public Test()
        {
            //建立委托
            TDelegate1 tg1 = new TDelegate1(Func1);
            //给委托加载方法
            tg1 += Func2;
            TDelegate2 tg2 = new TDelegate2(Func3);
            Func4(tg1,tg2,10);
        }

        public void Func1()
        {
            Console.WriteLine("Func1 Run");
        }

        public void Func2()
        {
            Console.WriteLine("Func2 Run");
        }

        public int Func3(int arg)
        {
            Console.WriteLine("Func3 Run");
            return arg;
        }
        /// <summary>
        /// 调用委托
        /// </summary>
        /// <param name="del1"></param>
        public void Func4(TDelegate1 del1,TDelegate2 del2,int args)
        {
            //委托需要判断该委托是否为空
            if (del1!= null)
            {
                del1.Invoke();
            }
            if (del2!= null)
            {
                var res = del2.Invoke(args);
                Console.WriteLine("res is {0}",res);
            }
        }
    }
}
