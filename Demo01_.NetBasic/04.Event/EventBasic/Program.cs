﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EventBasic
{
    //事件
    /*
     * 事件是委托的封装
     * 
     */
    //这是一个无入参无返回值委托
    //委托名称一般用动词
    public delegate void NonParamChange();
    //这是一个有入参有返回值委托
    public delegate int IntParamChange(int param);

    class Program
    {
        
        static void Main(string[] args)
        {
            var tevet = new TestEvent();
            tevet.OnNonChangeArgs += new EventHandler(tevet_OnNonChangeArgs);
            tevet.OnIntArgsChange +=new EventHandler<TestEventArgs>(tevet_OnIntArgsChange);
            tevet.OnParamChange += new NonParamChange(tevet_OnParamChange);
            tevet.OnIntParamChange += new IntParamChange(tevet_OnIntParamChange);
            tevet.InvokeEvent();
            Console.ReadLine();

        }

        static int tevet_OnIntParamChange(int param)
        {
            Console.WriteLine("param is {0}",param);
            return param+1;
        }

        static void tevet_OnParamChange()
        {
            Console.WriteLine("on param change invoke!");
        }

        static void tevet_OnNonChangeArgs(object sender, EventArgs e)
        {
            Console.WriteLine("non change args run");
        }

        static void tevet_OnIntArgsChange(object sender, TestEventArgs e)
        {
            Console.WriteLine("on int change args run \r\n name:{0} param:{1}",e.EventEventName,e.EventEventArg);
        }
    }

    class TestEvent
    {
        //事件名称需要On+委托名称
        public event NonParamChange OnParamChange;
        public event IntParamChange OnIntParamChange;
        //几个约定好的系统事件（带OBJECT参数的事件）
        public event EventHandler OnNonChangeArgs;
        public event EventHandler<TestEventArgs> OnIntArgsChange; 
        public TestEvent()
        {
            
        }

        public void InvokeEvent()
        {
            if (OnParamChange!= null)
            {
                OnParamChange.Invoke();
            }
            if (OnIntParamChange!= null)
            {
                OnIntParamChange.Invoke(100);
            }
            if (OnNonChangeArgs!= null)
            {
                OnNonChangeArgs.Invoke(this,new EventArgs());
            }
            if (OnIntArgsChange!= null)
            {
                OnIntArgsChange.Invoke(this,new TestEventArgs(){EventEventName = "arg1",EventEventArg = 100});
            }
        }

    }

    class TestEventArgs:EventArgs
    {
        public string EventEventName { get; set; }
        public int EventEventArg { get; set; }
        //定义一个事件参数
        public TestEventArgs()
        {
            
        }

        public TestEventArgs(string eventName,int eventArg)
        {
            EventEventName = eventName;
            EventEventArg = eventArg;
        }

    }
}
